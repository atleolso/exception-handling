# exception-handling

Code examples demonstrating exception-handling in Java:

- `Exceptions` has methods returning both checked and unchecked exceptions
- `Errors` has methods throwing unrecoverable errors
- `Asserts` demonstrates the use of the `assert` keyword