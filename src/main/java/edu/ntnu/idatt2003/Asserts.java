package edu.ntnu.idatt2003;

public class Asserts {

    public static void assertExample() {
        String text = "Lorem ipsum dolor sit amet";
        String textNoSpaces = text.replace(" ", "");

        assert textNoSpaces.contains(" ") : "Text contains spaces";
    }

    public static float celsiusToKelvin(float celsius) {
        float kelvin = celsius + 273.15f; //273.15 kelvin is freezing point of water.

        assert kelvin >= 0 : "A temp below absolute zero is impossible";
        return kelvin;
    }

    // If we can perform the same check without assert we should probably do so
    public static float celsiusToKelvinWithException(float celsius) throws IllegalArgumentException {
        final float freezingPointOfWaterInKelvin = 273.15f;
        if (celsius < -freezingPointOfWaterInKelvin) {
            throw new IllegalArgumentException("Temperature cannot be below absolute zero " +
                    "(-" + freezingPointOfWaterInKelvin + " C)");
        }
        return celsius + freezingPointOfWaterInKelvin;
    }

    public static void main(String[] args) {
        float celsius = -274.0f;
        float result = celsiusToKelvin(celsius);
        System.out.println("Celsius " + celsius + " is " + result + " kelvin");
    }
}
