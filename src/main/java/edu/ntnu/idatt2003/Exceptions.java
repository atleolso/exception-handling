package edu.ntnu.idatt2003;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Exceptions {

    public static float divideByZero() {
        return 10/0;
    }

    public static void readNonExistingFile() throws FileNotFoundException {
        FileReader fileReader = new FileReader("file.txt");
    }

    public static void main(String[] args) {
        try {
            float result = divideByZero();
        } catch (ArithmeticException e) {
            System.err.println("Dividing by zero is bad. Exception message: " + e.getMessage());
        }

        try {
            readNonExistingFile();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }
}
