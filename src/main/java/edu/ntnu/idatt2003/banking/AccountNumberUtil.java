package edu.ntnu.idatt2003.banking;

public class AccountNumberUtil {

    // Dummy method always returns true.
    // An actual implementation of this method would do a thorough check of the account number format.
    // For norwegian account numbers: https://no.wikipedia.org/wiki/Kontonummer#Norge
    public static boolean isValid(String accountNumber) {
        return true;
    }
}
