package edu.ntnu.idatt2003.banking;

import java.math.BigDecimal;

public class BankAccount {

    private final String accountNumber;
    private BigDecimal balance;

    public BankAccount(final String accountNumber, BigDecimal balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public BigDecimal withdraw(BigDecimal amount) throws InsufficientFundsException {
        if (balance.compareTo(amount) >= 0) {
            balance = balance.subtract(amount);
            return balance;
        } else {
            throw new InsufficientFundsException();
        }
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
