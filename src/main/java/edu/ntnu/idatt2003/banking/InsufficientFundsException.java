package edu.ntnu.idatt2003.banking;

public class InsufficientFundsException extends Exception {

    public InsufficientFundsException() {
        super("Insufficient funds in account");
    }
}
